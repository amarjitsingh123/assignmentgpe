using AssignmentGPE;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace UnitTesting
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// This test method checks that the values are updated correctly when drawTo is called.
        /// </summary>
        [TestMethod]
        public void drawToTest()
        {
            drawTo draw = new drawTo(200, 300);
            int actual1 = 200;
            int actual2 = 300;
            Assert.AreEqual(200, actual1);
            Assert.AreEqual(300, actual2);
        }

        /// <summary>
        /// This test method checks that if invalid parameters are given then the system outputs false.
        /// </summary>
        [TestMethod]
        public void drawToParamsInvalid()
        {
            drawTo draw = new drawTo(100, 100);
            string actual1 = "x";
            string actual2 = "y";
            Assert.AreNotEqual(100, actual1);
            Assert.AreNotEqual(100, actual2);
            Assert.IsFalse(actual1.Equals(100));
        }

        /// <summary>
        /// This test method checks that the moveTo command actually updates it's starting position correctly given the parameters.
        /// </summary>
        [TestMethod]
        public void moveToTest()
        {
            moveTo move = new moveTo(400, 300);
            int actual1 = 400;
            int actual2 = 300;
            Assert.AreEqual(400, actual1);
            Assert.AreEqual(300, actual2);
        }

        /// <summary>
        /// This test method checks that the moveTo parameters are reported false if invalid.
        /// </summary>
        [TestMethod]
        public void moveToParamsInvalid()
        {
            moveTo move = new moveTo(500, 600);
            string actual1 = "x";
            string actual2 = "x";
            Assert.AreNotEqual(500, actual1);
            Assert.AreNotEqual(600, actual2);
        }

        /// <summary>
        /// This test method makes sure that the parser method splits the initial string correctly.
        /// </summary>
        [TestMethod]
        public void parserInitialSplitTest()
        {
            string s = "moveto 100,100";
            string[] parameter = s.Split();
            Assert.IsTrue(parameter[0] == "moveto");
        }

        /// <summary>
        /// This test method makes sure that the paser method splits the second split correctly when defining parameters.
        /// </summary>
        [TestMethod]
        public void parserSecondSplitTest()
        {
            string s = "drawto 200,400";
            string[] userInput = s.Split();
            string[] parameter = userInput[1].Split(",");
            int count = parameter.Length;
            int[] parameters = new int[count];
            for (int i = 0; i < parameter.Length; i++)
            {
                parameters[i] = Convert.ToInt32(parameter[i]);
            }
            int actual, actual2;
            actual = 200;
            actual2 = 400;
            Assert.AreEqual(parameters[0], actual);
            Assert.AreEqual(parameters[1], actual2);
        }

        /// <summary>
        /// This test method makes sure that variables are being assigned as intended.
        /// </summary>
        [TestMethod]
        public void variableAssignTest()
        {
            List<string> varNames = new List<string>();
            List<int> varValues = new List<int>();
            string s = "num1 = 30";
            string[] userInput = s.Split();
            int varValue = Convert.ToInt32(userInput[2]);
            varValues.Add(varValue);
            varNames.Add(userInput[0]);
            string actual = varNames[0];
            int actual2 = varValues[0];
            Assert.AreEqual("num1", actual);
            Assert.AreEqual(30, actual2);
        }

        /// <summary>
        /// This test method checks that the variables are parsed into the program correctly.
        /// </summary>
        [TestMethod]
        public void variableParsingTest()
        {
            List<string> varNames = new List<string>() { "width" };
            List<int> varValues = new List<int>() { 50 };
            string s = "drawto width,400";
            string[] userInput = s.Split();
            string[] parameter = userInput[1].Split(",");
            int count = parameter.Length;
            int[] parameters = new int[count];
            for (int i = 0; i < parameter.Length; i++)                             
            {
                if (varNames.Contains(parameter[i]))
                {
                    int a = varNames.IndexOf(parameter[i]);
                    parameters[i] = varValues[a];
                }
                else
                {
                    parameters[i] = Convert.ToInt32(parameter[i]);
                }
            }
            int actual = varValues[0];
            string actual2 = varNames[0];
            Assert.AreEqual(50, actual);
            Assert.AreEqual("width", varNames[0]);
        }

        /// <summary>
        /// This test methods checks that variables can be added to when already stored in the system.
        /// </summary>
        [TestMethod]
        public void variableAdditionTest()
        {
            List<string> varNames = new List<string>() { "num1" };
            List<int> varValues = new List<int>() { 50 };
            string s = "num1 = num1 + 50";
            string[] userInput = s.Split();
            int Value = Convert.ToInt32(userInput[4]);
            int a = varNames.IndexOf(userInput[0]);
            varValues[a] = varValues[a] + Value;
            int actual = varValues[0];
            Assert.AreEqual(100, actual);
        }

        /// <summary>
        /// This test method checks that the variables can be subtracted from when already stored within the program.
        /// </summary>
        [TestMethod]
        public void variableSubtractionTest()
        {
            List<string> varNames = new List<string>() { "num1" };
            List<int> varValues = new List<int>() { 200 };
            string s = "num1 = num1 - 50";
            string[] userInput = s.Split();
            int Value = Convert.ToInt32(userInput[4]);
            int a = varNames.IndexOf(userInput[0]);
            varValues[a] = varValues[a] - Value;
            int actual = varValues[0];
            Assert.AreEqual(150, actual);
        }
        
        /// <summary>
        /// This test method makes sure that variables are able to add together using their alphabetical state successfully. e.g 'count = count + num1'.
        /// </summary>
        [TestMethod]
        public void variableAddingVariablesTest()
        {
            List<string> varNames = new List<string>() { "num1", "count" };
            List<int> varValues = new List<int>() { 200, 300 };
            string s = "num1 = num1 + count";
            s.ToLower();
            string[] userInput = s.Split();
            if (varNames.Contains(userInput[4]))
            {
                int varValue;
                int a = varNames.IndexOf(userInput[4]);
                varValue = varValues[a];
                int o = varNames.IndexOf(userInput[0]);
                varValues[o] = varValues[o] + varValue;

            }
            else
            {
                int varValue = Convert.ToInt32(userInput[4]);
                int a = varNames.IndexOf(userInput[0]);
                varValues[a] = varValues[a] + varValue;
            }

            int actual = varValues[0];
            Assert.AreEqual(500, actual);
        }
    }
}
